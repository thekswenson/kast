Fast computation of the Kernel of the maximum Agreement subtrees (KAST)
=======================================================================

This is C code developed by [Biing-Feng Wang](http://clark.cs.nthu.edu.tw/) that implements the algorithms discussed in "A Faster Algorithm for Computing the Kernel of Maximum Agreement Subtrees" for both the MAST and the KAST. There are also python and Snakemake scripts for testing the code on simulated data.

________________________________________________________________________
Using the code
--------------

To run the code you must follow these steps:

0. Install Git (in Ubuntu `sudo apt install git`).

1. Clone the repository:`git clone https://thekswenson@bitbucket.org/thekswenson/kast`

2. Compile
    1. `cd kast/runningtime` 
    2. Edit `kast.c` and change `MAX_K` (maximum number of trees), `MAX_N` (maximum number of leaves), and `MAX_D` (maximum number of children per node) to suite your needs. See the note below about memory usage for advice on setting these.
    3. Type one of the following:
        * `gcc -mcmodel=large -m64 -o kast_3000_10 kast.c`, or
        * `make`

3. Convert your input file from Newick to our .bfw format (this example uses `testcases/RAxML_bootstrap.00070.10.nwk`)
    * `newick2bfw.py testcases/RAxML_bootstrap.00070.10.nwk` produces the file `testcases/RAxML_bootstrap.00070.10.nwk.bfw`

4. Run the program
    * To compute the rooted MAST do `kast_3000_10 testcases/RAxML_bootstrap.00070.10.nwk.bfw 1 0`
    * To compute the UNrooted MAST do `kast_3000_10 testcases/RAxML_bootstrap.00070.10.nwk.bfw 0 0`
    * To compute the rooted KAST do `kast_3000_10 testcases/RAxML_bootstrap.00070.10.nwk.bfw 1 2`
    * To compute the UNrooted KAST do `kast_3000_10 testcases/RAxML_bootstrap.00070.10.nwk.bfw 0 2`
    
    (Putting 1 as the last argument, instead of 0 or 2, will invoke the old, slow, KAST algorithm)

### Interpreting the output

Consider the following sample run.

```
=> kast testcases/RAxML_bootstrap.00070.10.nwk.bfw 0 2

File: testcases/RAxML_bootstrap.00070.10.nwk.bfw, (tree_num, n, d) = (10, 49, 3)
Processing (UNROOTED KAST2: 1 rounds) ....
done.
mast = 12
kast = {5, [13][15][16][17][18]}

Time: (Exe (1 rounds), In/Out, Total) = (0.14046s, 0.00067s/0.00000s, 0.14113s)
```

The "kast" line is a tuple where the first value is the size of the KAST and the second value lists the leaves that are in the KAST.  To recover the names of the taxa associated with these leaf numbers, use the mapping file produced by newick2bfw.py when given the "-n" flag.

### Note on memory usage

Despite the setting of `MAX_K`, `MAX_N`, and `MAX_D`, the program will only use the amount of memory necessary for the computations. There are mostly no problems with setting those values too high. There are large limits, however, to how high you can set those parameters before the machine starts complaining.

________________________________________________________________________
Running the examples from the paper
-----------------------------------

### Simulations
To test the installation cd to the top directory and run `snakemake`.
To reproduce the plots from the paper run `snakemake --configfile snakeconfig/wander_100_150-1851_1_0.10_10_ROOT.yaml` for each of the files in the directory `sankeconfig/`.

### Ribosomal RNA data
The trees corresponding to the timings reported on rRNA alignments can be found at `runningtime/paper_trees`.

________________________________________________________________________
Installing dependencies
-----------------------

Our python scripts depend on Biopython and Snakemake. On Ubuntu, run `sudo apt install git python3-biopython snakemake python3-dendropy python3-matplotlib` to install them.
