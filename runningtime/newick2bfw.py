#!/usr/bin/env python3
"""
Convert a newick file to the Biing-Feng format.
"""
import sys, os
import argparse

import dendropy
from dendropy.datamodel import treemodel

#Globals:

be_verbose = False

#        _________________________
#_______/        Functions        \_____________________________________________


def newick2bfw(newickfile, bfwfile, mappingfile=''):
  """
  Convert the given newick file to the Biing-Feng Wang format.

  The format is as follows.
  The first line contains two numbers k and n, indicating there are k trees
  with n leaves.

  Then, k trees follow, each of which is given as follows.
  (a) The first line gives the number m.
  (b) The next m lines give the children of each internal nodes.
  
  The following is a sample input.
  
  2 5     /* k = 2 and n =5 */
  4       /* 1st tree has m = 4 internal nodes with labels 5 (root), 6, 7, 8
  2 6 7   /* node 5 has two children 6, 7
  2 8 2   /* node 6 has two children 8, 2
  2 3 4   /* node 7 has two children 3, 4
  2 0 1   /* node 8 has two children 0, 1
  3       /* 2nd tree has m = 3 internal nodes with labels 5 (root), 6, 7
  2 6 7   /* node 5 has two children 6, 7
  3 0 1 2 /* node 6 has three children 0, 1, 2
  2 3 4   /* node 7 has two children 3, 4

  @param newickfile:  file with Newick trees in it.
  @param bfwfile:     output BFW format to this file.
  @param mappingfile: write the integer to name mapping to this file.
  """
  with open(bfwfile, 'w') as f:
    if not os.path.exists(newickfile):
      sys.exit('ERROR: file {} does not exist.')

    with open(newickfile) as inf:
      f.write(newickStr2bfw(inf.read(), mappingfile))

  

def newickStr2bfw(newickstr, mappingfile=''):
  """
  Convert the given newick string to Biing-Feng Wang format.

  The format is as follows.
  The first line contains two numbers k and n, indicating there are k trees
  with n leaves.

  Then, k trees follow, each of which is given as follows.
  (a) The first line gives the number m.
  (b) The next m lines give the children of each internal nodes.
  
  The following is a sample input.
  
  2 5     /* k = 2 and n =5 */
  4       /* 1st tree has m = 4 internal nodes with labels 5 (root), 6, 7, 8
  2 6 7   /* node 5 has two children 6, 7
  2 8 2   /* node 6 has two children 8, 2
  2 3 4   /* node 7 has two children 3, 4
  2 0 1   /* node 8 has two children 0, 1
  3       /* 2nd tree has m = 3 internal nodes with labels 5 (root), 6, 7
  2 6 7   /* node 5 has two children 6, 7
  3 0 1 2 /* node 6 has three children 0, 1, 2
  2 3 4   /* node 7 has two children 3, 4

  @param newickstr:   tree in newick format.
  @param mappingfile: write the integer to name mapping to this file.
  """
    #Make sure there are no bad characters in the newick string:
  newickstr = newickstr.replace('=', '_')

  retstring = ''
  rooting = 'default-rooted'
  #rooting = 'force-unrooted'
  treelist = dendropy.TreeList.get(data=newickstr, schema='newick',
                                   rooting=rooting)


  mapfile = None
  if mappingfile:
    mapfile = open(mappingfile, 'w')

    #Rename leaves from 0 to n-1.
  numleaves = 0                 #The number of leaves n.
  for leaf in treelist.taxon_namespace:
    if mapfile:
      mapfile.write(f'{numleaves}, "{leaf.label}"\n')

    leaf.label = str(numleaves)
    numleaves += 1

  if mapfile:
    mapfile.close()

  retstring += f'{len(treelist)} {numleaves}\n'
  for tree in treelist:
    numinternal = len(tree.internal_nodes())
    if be_verbose:
      print(numinternal)
    retstring += f'{numinternal}\n'

      #Label the nodes:
    nodecounter = numleaves    #Internal node labels start at n (root is n).
    for node in tree.preorder_node_iter():
      if node.is_leaf():
        node.label = node.taxon.label
      else:
        node.label = nodecounter
        nodecounter += 1

      #Print the tree:
    nodecounter = numleaves
    for node in tree.preorder_node_iter():
      if not node.is_leaf():
        assert(nodecounter == node.label)
        nodecounter += 1

        children = node.child_nodes()
        retstring += f'{len(children)} '

        for c in children:
          retstring += f'{c.label} '

        retstring += '\n'

  return retstring



################################################################################
################################################################################

def main():
  global be_verbose

  desc = 'Convert a Newick tree file into Biing-Feng format.'
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('NEWICK_FILE', nargs='+',
                      help='the Newick file to convert')

  parser.add_argument('-n', '--name-mapping', metavar='NAME_FILE',
                      help='save the mapping of integers to names in the given (.csv) file.')
  parser.add_argument('-v', action='store_true',
                      help='be verbose.')
  
  args = parser.parse_args()

  in_files = args.NEWICK_FILE
  be_verbose = args.v
  save_mapping = args.name_mapping



#        ______________________
#_______/    Do Everything     \________________________________________________


  for infile in in_files:
    newick2bfw(infile, f'{infile}.bfw', save_mapping)

if __name__ == "__main__":
  main()
