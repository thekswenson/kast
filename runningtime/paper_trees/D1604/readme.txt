Dataset from the paper by Simon A. Berger, Denis Krompass, and Alexandros Stamatakis.

  "Performance, Accuracy, and Web Server for Evolutionary Placement of Short Sequence Reads under Maximum Likelihood".

  https://academic.oup.com/sysbio/article/60/3/291/1667010
