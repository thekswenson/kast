The alignment was downloaded from:

  http://www.rna.icmb.utexas.edu/DAT/3C/Alignment/Files/16S/16S.B.ALL.alnfasta.zip

The first 15,000 rows of the alignment were selected. After identical rows were
remove by RAxML, there were 13073 left.
