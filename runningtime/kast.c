
/* KAST-v4 is a modification of KAST-v3, which allows unrooted input trees  */
/* KAST-v3 is a further space-reduced version of KAST-v2, which removes tables RTriple/FTriple by
   using two functions Rtriple/Ftriple */
/* Currently, only the modifications of KAST1 and KAST2 are done  */
/* KAST-v2 is a space-reduced version of KAST-v1, which tries to reduce the space of K and K1 by
   dynamically allocating space to a set of variable size */
/* KAST-v1 is the same with KAST20, which is the last version of codes that without version id */

/* Usage: KAST Input_File tree_type function2run */

/* tree_type: 0/1 for unrooted/rooted trees; function2run: 0/1/2 for MAST/KAST1/KAST2 */

/* Sample of Input_File: sample.txt 0
2 5     (k, n)
4       (m0=4 internal nodes 5 (root, n), 6, 7, 8)
2 6 7   (5 has two children 6, 7)
2 8 2
2 3 4
2 0 1
3       (m1=3 internal nodes 5 (root), 6, 7)
2 6 7   (5 has two children 6, 7)
3 0 1 2
2 3 4
*/

/* The following speedup may be considered
-- a more efficient way for finding a max clique (e.g., not consider leaves in the same subtree, B&B)
-- check whether all K(a, b) with m(a, b) = mast are the same, avoiding intersection; done for KAST2
-- check whether all K1 of all U(c) are the same, avoiding intersection
-- not explore (c, a) if (a, b) has no valid subtree (i.e., ST_validT[a][b] = 2)
-- record the largest size of a max clique in C(a, b) to speedup next_maxclique (good for large max_d)
-- FindRelevant needs Cmax as local, which may cases a problem for stack when n is large
-- KAST2 can used concate instead of union
*/

/* Remark: To demonstrate the speedup, it is better to have small k, small d, and very similar trees (large KAST). */
/* Speedup should be 8/n for balanced binary trees (KAST1: n^4/16; KAST2: n^3/2)*/

/* =============== constants ===============*/

#define MAX_K 100             /* max number of trees;*/
#define MAX_N 3000            /* max number of leaves; <= 2^16, otherwise SET_mul_intersec fail */
#define MAX_D 3               /* bound on the smallest max degree */
#define Default_tree_type  1  /* 0 for unrooted; 1 for rooted */
#define Default_Function2Run  2     /* 0 for MAST; 1 for KAST1; 2 for KAST2 */

#define SETPOOLSIZE 2000000 /* asked for 2M LABELs each time */
#define SET_POOL_MAXLEN 10000
#define LABEL short     /* type of leave LABELs */


#define TESTING 0      /* >=1 for testing set_op_cnt; >=2 for printing K(a, b); >=3 for printing K1(a, b)*/
#define DISPLAYROOT 0      /* 1 for displaying the current root */
#define default_test_rounds 1  /* round of rounds to be performed; used when n is small */

/* ============= output file name =====*/
#define OutputFileName "result.txt"   /* currently, there is no file output */


/* ============= default input files, which are used for testing or no argu is given =====*/
#define Default_Input 100330   /*147  404 592 901 1937*/

#if (Default_Input==10022)
  char Default_InputFileName[] = "100_22.bfw";
#endif
#if (Default_Input==100330)
  char Default_InputFileName[] = "100_330.bfw";
#endif
#if (Default_Input==0)
  char Default_InputFileName[] = "ttt.txt";
#endif

#if (Default_Input==1)
  char Default_InputFileName[] = "sample00a.txt";
#endif
#if (Default_Input==2)
  char Default_InputFileName[] = "first2of404.txt";
#endif
#if (Default_Input==147)
  char Default_InputFileName[] = "RAxML_bootstrap_input00147.txt";
#endif
#if (Default_Input==4043)
  char Default_InputFileName[] = "404x3.txt";
#endif
#if (Default_Input==4042)
  char Default_InputFileName[] = "404x2.txt";
#endif
#if (Default_Input==404)
  char Default_InputFileName[] = "RAxML_bootstrap_input00404.txt";
#endif
#if (Default_Input==592)
  char Default_InputFileName[] = "RAxML_bootstrap_input00592.txt";
#endif
#if (Default_Input==5922)
  char Default_InputFileName[] = "592x2.txt";
#endif
#if (Default_Input==9012)
  char Default_InputFileName[] = "901x2.txt";
#endif
#if (Default_Input==901)
  char Default_InputFileName[] = "RAxML_bootstrap_input00901.txt";
#endif
#if (Default_Input==1937)
  char Default_InputFileName[] = "RAxML_bootstrap_input01937.txt";
#endif
#if (Default_Input==15000)
  char Default_InputFileName[] = "RAxML_bootstrap_input15000.txt";
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>


/* =============== larger size DS needs malloc =========== */
typedef struct {int size; LABEL memb[MAX_N];} SET; /* set of labels */
typedef struct {int size; LABEL *memb;} DSET; /* set (with dynamic size) of labels */

DSET  (*K[MAX_N]), (*K1[MAX_N]); /*for K(a, b) and K(a|b)*/

/* ===============DS for trees =========== */
/* k trees of n leaves; # of internal nodes: m0, m1, ..., m(k-1) */
/* leaves are 0 ~ n-1; internal nodes of T0 are n ~ n+m0-1; internal nodes of T1 are n + m0 ~ n + m0 + m1 -1; and so on */
int n, tree_num, max_d, T0, root0, newroot0;  /* tree_num = k; T0 is id of the tree with degree max_d; root0 is the root of T0*/
int treestart[MAX_K+1]; /*root of Ti is n + m0 + m1 + ... + m(i-1) *//* needs +1 for sentinel */
int leafroot;
typedef struct {LABEL degree; int first;} TREENODE; /* childlist[first], childlist[first+1], ..., childlist[first+degree-1] are its children */
TREENODE node[MAX_N+MAX_N*MAX_K]; /* each node records its 1st child in childlist (the last one is its parent in the input tree);  reserve 0 ~ n-1 for leaves */
int leafpar[MAX_K][MAX_N];
int childlist[3*MAX_N*MAX_K]; /* pool of child-list; each internal records its adjacency list  */

/* =============== DS for find max clique =============================== */
LABEL Csize, C[MAX_N], W[MAX_N]; /* C[0] is not used */
LABEL MaxCweight, prefixweight;
int cliq[MAX_D+1], cliq_id[MAX_D+1], cliqlen; /* cliq[0] is not used*/
int max_cliq_size;

/* =============== DS for MAST =============================== */
LABEL M[MAX_N][MAX_N], M1[MAX_N][MAX_N], MC[MAX_N][MAX_N]; /*for m(a, b), m(a|b), and m(a, b)-m(a|b)-m(b) */
     /* MC[a][b] = 0 means C(a, b) = 0, i.e., binary */

SET Lset[MAX_N-1];   /* Lset[i-root[T0]] stores the leaf set (from left to right) of node i of T0; */
                     /* also used as temp for DFS*/
LABEL lca[MAX_N][MAX_N];        /* lca(a, b) in T0; only for a ! b*/

char LSubT[MAX_N-1][MAX_N];     /* if c is a leaf of T[v], LSubT[v][c] = i (>=0) indicates c is in the i-th subtree of v */

LABEL mast = 0;

/* =============== DS KAST1 =================== */
SET kast; /* the answer */

/* ============ DS for KAST2 =================== */
char Relev[MAX_N][MAX_N];     /* 1 if (a, b) is relevant */

typedef struct {LABEL a, b, m;} TRIPLE;

TRIPLE order[MAX_N*MAX_N]; int order_len; /* order in size of m(a, b) */
LABEL *CH[MAX_D+1]; /* CH[i] pointers to the 1st C(a, b, H) in C(a, b), where CH[MAX_D] is a sentinel*/

#define KK1 K1
/* KK1[a][t] = intersection of K(a, x) with m(a, x) = t = K1[a][b] for m(a|b) = t */
/* since KK1[a][m1[a][b]] = K1[a][b]; K1 is unnecessary; let KK1 use the space of K1*/

DSET *dmul_s[MAX_N*MAX_N]; int dmul_s_len; /* used for mul-dset op in FINDKAST/KAST2/KAST/FINSKAST2/FINDVALID/FINDVALID1*/

/* ============ DS for KAST3 =================== */
// SET TABLESET[MAX_N][MAX_N];
#define TABLESET K          /* K is unnecessary; let KK1 use the space of K1*/
SET K_ab;                   /* K(a, b) only used once, use K_ab to replace it  */
SET *mul_ab[MAX_N]; LABEL mul_ab_len; /* GOLBAL OUTPUT of FindValid, used in KAST3, KAST4*/

/* ============ DS for KAST4 =================== */
/* the following are computed only for a<b */
char ST_validT[MAX_N][MAX_N][MAX_D]; /* valid[a][b] */

int Usize;  LABEL U[MAX_N*MAX_N]; /* pool for max c in C(a, b, H) when (a, b) is valid */
int U_start[MAX_N][MAX_N][MAX_D], U_endplus1[MAX_N][MAX_N][MAX_D];

/* ============== MAINTAIN DSETS (sets of variable sizes) ============= */
LABEL *SETPOOL[SET_POOL_MAXLEN]; int SETPOOL_len, SETPOOL_free; /* SEPPOOL[0] is unused */

void DSET_print(char msg[] , DSET *s);

void AddSETPOOL()
{
    SETPOOL[++SETPOOL_len] = malloc(SETPOOLSIZE*sizeof(LABEL));
    if (SETPOOL[SETPOOL_len] != NULL)
    {
       SETPOOL_free = 0; return;
    }
    printf("\n\n....Insufficient of memory for tables K and K !!!\n");
    printf("....Please terminate this process (press Ctr-C)\n");
    while (1); /* waiting for termination */
}

void init_SETPOOL() {SETPOOL_len = 0; AddSETPOOL();}

void free_SETPOOL() {int i; for (i = SETPOOL_len; i > 0; ) free(SETPOOL[i--]); }

void DSET2SET(DSET *s1, SET *s2)
{
    s2->size = s1->size;
    memcpy(s2->memb, s1->memb, s1->size*sizeof(LABEL));
}

void SET_KandK1(DSET *s1, SET *s2)
/* s1 is a DSET of K/K1; s2 is a SET; */
{
    if (SETPOOL_free + s2->size > SETPOOLSIZE) AddSETPOOL();
    s1->memb = SETPOOL[SETPOOL_len] + SETPOOL_free;
    memcpy(s1->memb, s2->memb, s2->size*sizeof(LABEL));
    SETPOOL_free += (s1->size = s2->size);
}

void SET_KandK1_1(DSET *s1, LABEL x)
/* s1 is a DSET of K/K1; s2 is a SET; */
{
    if (SETPOOL_free + 1 > SETPOOLSIZE) AddSETPOOL();
    s1->memb = SETPOOL[SETPOOL_len] + SETPOOL_free;
    s1->memb[0] = x; s1->size = 1;
    SETPOOL_free++;
}

/* ============== SET OPERATIONS ============= */
#if (TESTING>=1)
   unsigned long set_op_cnt, set_op_total_size;
#endif

int SET_flag[MAX_N]; /* used to speedup set operations; when MAX_N >= 2^16 SET_mul_intersect done */

void SET_init(int n) {memset(SET_flag, 0, n);} /* initialize flags, where n is the num of leaves */

int my_comp_LABEL(const void *a, const void *b)
{
    return (*(LABEL *)a - *(LABEL *)b);
}

void DSET_print(char msg[] , DSET *s)
{ int i; SET t;
  memcpy(t.memb, s->memb, s->size*sizeof(LABEL));
  qsort(t.memb, s->size, sizeof(LABEL), my_comp_LABEL);
  printf("%s", msg); printf("{%d, ", s->size);
  for (i=0; i < s->size; i++) printf("[%d]", t.memb[i]);
  printf("}\n");
}


void SET_print(char msg[] , SET *s)
{ int i; SET t;
  memcpy(t.memb, s->memb, s->size*sizeof(LABEL));
  qsort(t.memb, s->size, sizeof(LABEL), my_comp_LABEL);
  printf("%s", msg); printf("{%d, ", s->size);
  for (i=0; i < s->size; i++) printf("[%2d]", t.memb[i]);
  printf("}\n");
}

void SET_copy(SET *s1, SET *s2)
/* assume that s1 and s2 are disjoint */
{
    s1->size = s2->size;
    memcpy (s1->memb, s2->memb, s1->size*sizeof(LABEL));
}

char SET_equal(SET *s1, SET *s2)
{   int i; SET t1, t2;

    if ((s1->size) != (s2->size)) return 0;
    memcpy(t1.memb, s1->memb, s1->size*sizeof(LABEL));
    qsort(t1.memb, s1->size, sizeof(LABEL), my_comp_LABEL);
    memcpy(t2.memb, s2->memb, s2->size*sizeof(LABEL));
    qsort(t2.memb, s2->size, sizeof(LABEL), my_comp_LABEL);
    for (i=0; i < s1->size; i++)
        if ((t1.memb[i])!= (t2.memb[i])) return 0;
    return 1;  /* the same */
}

void SET_intersec(SET *s1, SET *s2, SET *s)
{   int i, newsize = 0;
    for (i=0; i< s1->size; i++) SET_flag[s1->memb[i]] = 1;
    for (i=0; i< s2->size; i++)
        if (SET_flag[s2->memb[i]]) s->memb[newsize++] = s2->memb[i];
    for (i=0; i< s1->size; i++) SET_flag[s1-> memb[i]] = 0; /* clear flags */
    s->size =  newsize;
#if (TESTING>=1)
    set_op_cnt++; set_op_total_size+=(s1->size+s2->size);
#endif
}

void DSET_intersec1(DSET *s1, SET *s2)
/*store result in s1 */
{   int i, newsize = 0;
    for (i=0; i< s2->size; i++) SET_flag[s2->memb[i]] = 1;
    for (i=0; i< s1->size; i++)
        if (SET_flag[s1->memb[i]]) s1->memb[newsize++] = s1->memb[i];
    for (i=0; i< s2->size; i++) SET_flag[s2-> memb[i]] = 0; /* clear flags */
    s1->size =  newsize;
#if (TESTING>=1)
    set_op_cnt++; set_op_total_size+=(s1->size+s2->size);
#endif
}

void SET_intersec1(SET *s1, SET *s2)
/*store result in s1 */
{   int i, newsize = 0;
    for (i=0; i< s2->size; i++) SET_flag[s2->memb[i]] = 1;
    for (i=0; i< s1->size; i++)
        if (SET_flag[s1->memb[i]]) s1->memb[newsize++] = s1->memb[i];
    for (i=0; i< s2->size; i++) SET_flag[s2-> memb[i]] = 0; /* clear flags */
    s1->size =  newsize;
#if (TESTING>=1)
    set_op_cnt++; set_op_total_size+=(s1->size+s2->size);
#endif
}

void DSET_mul_intersec(int m, DSET **mul_s, SET *s)
{
    int i, k, newsize = 0, m1 = m-1;

    for (k = 0; k < m1; k++)     /* counting */
      for (i = 0; i < mul_s[k]->size; i++) SET_flag[mul_s[k]->memb[i]]++;

    for (i =0 ; i < mul_s[m1]->size; i++) /* find intersection */
      if (SET_flag[mul_s[m1]->memb[i]]== m1) s->memb[newsize++] = mul_s[m1]->memb[i];

    for (k = 0; k < m-1; k++)     /* clear flags */
      for (i=0; i < mul_s[k]->size; i++) SET_flag[mul_s[k]->memb[i]]=0;

    s->size =  newsize;

#if (TESTING>=1)
    set_op_cnt+=m;  for (k = 0; k < m; k++) set_op_total_size+=mul_s[k]->size;
#endif
}

void DSET_concate(DSET *s1, DSET *s2, SET *s)
/* assume that s1 and s2 are disjoint */
{
    s->size = s1->size + s2->size; /* size of s */
    memcpy (s->memb, s1->memb, s1->size*sizeof(LABEL)); /*add s1 */
    memcpy (s->memb + s1->size, s2->memb, s2->size*sizeof(LABEL)); /*add s2 */
}

void DSET_mul_concate(int m, DSET **mul_s, SET *s)
/* assume that all sets are disjoint */
{   int k, newsize = 0;
    LABEL *ss;
    ss = s->memb;
    for (k = 0; k < m; k++)
    {
       memcpy (ss+newsize, mul_s[k]->memb, mul_s[k]->size*sizeof(LABEL)); /*add mul_s[k] */
       newsize+=mul_s[k]->size;
    }
    s->size =  newsize;
#if (TESTING>=1)
    set_op_cnt+=m; for (k = 0; k < m; k++) set_op_total_size+=mul_s[k]->size;
#endif
}

void DSET_union(DSET *s1, DSET *s2, SET *s)
/* s1 and s2 may not be disjoint */
{
    int i, newsize = s1->size;
    memcpy (s->memb, s1->memb, s1->size*sizeof(LABEL)); /*add s1 */
    for (i=0; i< s1->size; i++) SET_flag[s1->memb[i]] = 1;
    for (i=0; i< s2->size; i++) /* add s2 */
        if (SET_flag[s2->memb[i]] == 0) s->memb[newsize++] = s2->memb[i];
    for (i=0; i< s1->size; i++) SET_flag[s1->memb[i]] = 0; /* clear flags */
    s->size = newsize; /* size of s */
    /*printf("SET union here \n");
    SET_print("S1 ", s1); SET_print("S2", s2); SET_print("S", s);*/
#if (TESTING>=1)
    set_op_cnt++;  set_op_total_size+=(s1->size+s2->size);
#endif
}

void SET_union(SET *s1, SET *s2, SET *s)
/* s1 and s2 may not be disjoint */
{

    int i, newsize = s1->size;
    memcpy (s->memb, s1->memb, s1->size*sizeof(LABEL)); /*add s1 */
    for (i=0; i< s1->size; i++) SET_flag[s1->memb[i]] = 1;
    for (i=0; i< s2->size; i++) /* add s2 */
        if (SET_flag[s2->memb[i]] == 0) s->memb[newsize++] = s2->memb[i];
    for (i=0; i< s1->size; i++) SET_flag[s1->memb[i]] = 0; /* clear flags */
    s->size = newsize; /* size of s */
    /*printf("SET union here \n");
    SET_print("S1 ", s1); SET_print("S2", s2); SET_print("S", s);*/
#if (TESTING>=1)
    set_op_cnt++;  set_op_total_size+=(s1->size+s2->size);
#endif
}

void SET_mul_union(int m, SET **mul_s, SET *s)
/* sets may not be disjoint */
{   int k, i, newsize = 0;

    for (k = 0; k < m; k++) /* add mul_S[k] */
      for (i = 0; i < mul_s[k]->size; i++)
        if (SET_flag[mul_s[k]->memb[i]] == 0)
        {
            s->memb[newsize++] = mul_s[k]->memb[i];
            SET_flag[mul_s[k]->memb[i]] = 1;
        }

    for (k = 0; k < m; k++) /* clear flag */
      for (i = 0; i < mul_s[k]->size; i++) SET_flag[mul_s[k]->memb[i]] = 0;

    s->size =  newsize; /* size of s */
#if (TESTING>=1)
    set_op_cnt+=m; for (k = 0; k < m; k++) set_op_total_size+=mul_s[k]->size;
#endif
}

/* =============== Input/Output =========================== */

void print_tree(int t)
{ int offset = treestart[t], endplus = treestart[t+1];
  int i, v, child;
  printf("Tree %d is as follows:\n", t);
  for (v=offset; v<endplus; v++)
  { printf("child of %d: ", v-offset+n);
    for (i = 0; i < node[v].degree; i++)
    {
        child = childlist[node[v].first + i];
        if (child < n) printf("[%d]", child);
        else printf("[%d]", child-offset+n);
    }
    printf("\n");
  }
}

int par[MAX_N-1]; char color[MAX_N-1];

void DFS_FindPar(int offset, int v)
/* v is an internal */
{   int i, child;
    color[v-offset] =  1;  /* v is visited */
    for (i = 0; i < node[v].degree; i++)
    {
        child = childlist[node[v].first + i];
        if (child < n) continue; /* a leaf child */
        if (!color[child-offset]) /* an unvisited internal child */
        {
           par[child-offset] = v;
           DFS_FindPar(offset, child);
        }
    }
}

int newroot[MAX_K];

void init_rooted_trees()
/* produce rooted trees with root f; */
{ int i;

  for (i=0; i<tree_num; i++) newroot[i] = treestart[i];
  newroot0=newroot[T0];
}


void init_unrooted_trees()
{
  int i, j;
  for (i=0; i<tree_num; i++)
      for (j=treestart[i] +  1; j<treestart[i+1]; j++) node[j].degree++;
}

void toRootedTrees(LABEL f)
/* produce rooted trees with root f; */
{ int i, j, c, start, endplus;

  for (i=0; i<tree_num; i++)
  {
      start = treestart[i]; endplus = treestart[i+1];
      memset(color, 0, endplus - start);

      newroot[i] = leafpar[i][f]; /* parent of leaf f is the root */

      par[newroot[i]-start] = f;
      DFS_FindPar(start, newroot[i]);
      for (j=start; j<endplus; j++)
      { int c1st = node[j].first, cend = node[j].first + node[j].degree - 1;

        for (c=c1st; c <= cend; c++) /* "remove" parent from childlist */
          if (childlist[c] == par[j-start])
          { int t;
            t=childlist[cend];  childlist[cend] = childlist[c];  childlist[c] =  t;
            node[j].degree--;
            break;
          }
      }
  }
  newroot0=newroot[T0];leafroot = f;

}

void toRootedTreesRecover()
/* produce rooted trees with root f; */
{ int i, j;

  for (i=0; i<tree_num; i++)
      for (j=treestart[i]; j<treestart[i+1]; j++) node[j].degree++;
}

char get_input(char *filename)
/* pre-compute the above; reject (with error message) if k, n, max_d larger than MAX_N, MAX_K, MAX_D */
/* return 0 if succeed  */
{   int par[2*MAX_N]; /* parents; also used to check whether the input are trees */

    FILE* fp = fopen(filename, "r");
    if (fp==NULL) {
        printf("%s does not exist !!!\n", filename);
        return 1;
    }

    if (fscanf(fp, "%d%d", &tree_num, &n)!=2){
            printf("The input file is incorrect!\n");
            return 1;
    }

    if (tree_num>MAX_K) {
        printf("The number of trees is larger than %d!\n", MAX_K);
        return 1;
    }
    if (n > MAX_N){
        printf("The number of leaves is larger than %d!\n", MAX_N);
        return 1;
    }
    int childNum = 0; int nodeNum = n; int sumInternal = n; int maxDegree = -1;
    T0 = 0; max_d = MAX_D+1; int i, j, x;

    for(i = 0;i < tree_num;i++){
        int internalNum;

        if (fscanf(fp, "%d", &internalNum)!=1){
            printf("The input file is incorrect!\n");
            return 1;
        }

        {  int v;
           for (v=0; v < n+internalNum; v++) par[v] = -1;  /* for checking; > 0 means appear */
           par[n] = n;
        }

        treestart[i] = sumInternal;
        maxDegree = -1;
        for(j = 0;j < internalNum;j++){
            if (fscanf(fp, "%hd", &node[nodeNum].degree)!=1){
                printf("The input file is incorrect!\n");
                return 1;
            }
            if(node[nodeNum].degree > MAX_D){
                printf("The smallest maximum degree of the %d-th trees (%d) is larger than %d!\n", i+1, node[nodeNum].degree, MAX_D);
                return 1;
            }
            if(node[nodeNum].degree > maxDegree){
                maxDegree = node[nodeNum].degree;
            }
            node[nodeNum].first = childNum;
            for(x = 0;x < node[nodeNum].degree;x++){
                LABEL child;

                if ( fscanf(fp, "%hd", &child)!=1){
                printf("The input file is incorrect!\n");
                return 1;
                }

                par[child] = nodeNum;  /* this node appear; record its parent */

                if(child < n) childlist[childNum] = child;
                else childlist[childNum] = child-n+sumInternal;
                childNum++;
            }

            if (nodeNum != treestart[i]) childNum++; /* reserved for par */
            //if (nodeNum != treestart[i]) {childNum++; node[nodeNum].degree++;}
                 /* non-root node needs a space to record its parent */
            nodeNum++;
        }


        { int v;
          for (v=0; v<n+internalNum; v++)
          {
              if (v == n) continue;
              if (par[v] == -1) {
                printf("The %d-th input tree is invalid (NOT a TREE: without node %d)!!!!\n", i+1, v);
                return 1;
              }
              //printf("par[%d] = %d\n", v, par[v]);
              if (v<n) leafpar[i][v] = par[v];
              else childlist[node[v-n+treestart[i]].first+node[v-n+treestart[i]].degree] = par[v];
          }
        }

        sumInternal += internalNum;
        if(maxDegree < max_d){
            max_d = maxDegree;
            T0 = i;   root0 = treestart[T0]; /* record T0 */
        }
    }

    treestart[tree_num] = sumInternal; /* for knowing the size of T[tree_num-1] */
    return 0; /* succeed */
}


/* ---------------------------- */
void output() /* to be implemented */
{


}
/* ============ Computing rooted/fan triples, leaf sets, and lca ============= */
void printlca()
{ LABEL s, a, b, x, root = treestart[T0];
    for (a=0; a<n; a++)
    {
         for (b=0; b<n; b++) if (a!=b) printf("lca(%d, %d) = %d;", a, b, lca[a][b]);
         printf("\n");
    }

    for (s=root; s < treestart[T0+1]; s++)
        for (x =0; x< Lset[s-root].size; x++)
          printf("%d is in the %d-th subtree of %d\n", Lset[s-root].memb[x], LSubT[s][Lset[s-root].memb[x]], s);
}

/* =============== New DS for triples =============================== */
LABEL lcaAll[MAX_K][MAX_N][MAX_N];
    /* lcaAll[k](a, b) in Tk; only for a ! b*/

void ComputeLCA_of_Tk(int k)
/* Assume Lset is ready for Tk; find lcaAll[k] */
{ SET L1, L2, *p1, *p2; int s, i, j, y, z;
  int root = treestart[k], lastplus = treestart[k+1];
  LABEL (*lca_k)[MAX_N] = lcaAll[k];
  int child1, child2; LABEL a;

  L1.size = L2.size = 1;

  for (s=root; s < lastplus; s++) /* lca of internal s */
  {
     for(i = 0; i < node[s].degree; i++){
        if ((child1 = childlist[node[s].first+i])<n) {L1.memb[0] = child1; p1=&L1;} else p1 = &Lset[child1-root];
        for (y = i + 1; y < node[s].degree; y++){
            if ((child2 = childlist[node[s].first+y])<n) {L2.memb[0] = child2; p2=&L2;} else p2 = &Lset[child2-root];

            for(j = 0; j < p1->size; j++){
                 a = p1->memb[j];
                 for(z = 0; z < p2->size;z++) lca_k[p2->memb[z]][a] = lca_k[a][p2->memb[z]]= s;

            }
        }
    }
  }
  for (i=0; i<n; i++) lca_k[i][i] = i;
}

void ComputeLCA_of_T0()
/* Assume Lset is ready for T0; find lca and LsubT */
{ SET L1, L2, *p1, *p2; int s, i, j, y, z;
  int lastplus = treestart[T0+1];
  int child1, child2; LABEL a, b;

  L1.size = L2.size = 1;

  for (s=root0; s < lastplus; s++) /* lca of internal s */
  {
     for(i = 0; i < node[s].degree; i++){
        if ((child1 = childlist[node[s].first+i])<n) {L1.memb[0] = child1; p1=&L1;} else p1 = &Lset[child1-root0];

        for (j = 0; j < p1->size; j++) LSubT[s-root0][p1->memb[j]] = i;    /* p1->memb[j] is in the i-th subtree of s */

        for (y = i + 1; y < node[s].degree; y++){
            if ((child2 = childlist[node[s].first+y])<n) {L2.memb[0] = child2; p2=&L2;} else p2 = &Lset[child2-root0];

            for(j = 0; j < p1->size; j++){
                 a = p1->memb[j];
                 for(z = 0; z < p2->size;z++) lca[p2->memb[z]][a] = lca[a][p2->memb[z]]= s;

            }
        }
    }
  }

  for (a=0; a<n; a++) for (b=a+1; b<n; b++) lcaAll[T0][a][b]= lcaAll[T0][b][a]= lca[a][b];
  for (i=0; i<n; i++) lcaAll[T0][i][i] = i;
}

int dfs_order[MAX_N], dfs_len; /* for performing DFS on T0 */

void print_Lset(int k)
{
    int s, i;
    for (s=treestart[k]; s < treestart[k+1]; s++)
    { printf("Lset[%d] = ", s - treestart[k]+n);
      for(i = 0; i < Lset[s-treestart[k]].size; i++)
        printf("[%d]", Lset[s-treestart[k]].memb[i]);
      printf("\n");
    }

}

void makeLset(int root, int s)
/* s is internal node; compute leaf set L(s); note that L(s) is stored at Lset[s-root] */
{

    int ss = s-root, child, cc, i, j;
    Lset[ss].size = 0;
    for(i = 0; i < node[s].degree; i++){
        child = childlist[node[s].first+i];
        if (child < n) Lset[ss].memb[Lset[ss].size++] = child;
        else {
            makeLset(root, child);
            cc = child-root;
            for(j = 0;j < Lset[cc].size; j++)
              Lset[ss].memb[Lset[ss].size++] = Lset[cc].memb[j];

        }
    }
    dfs_order[dfs_len++]= s;
}

char Rtriple(LABEL b, LABEL a, LABEL x)
/* check whether ax|b *//* note that return true when a = x */
{ int k; int ab, bx, ax;
  for (k=0; k<tree_num; k++)
  {
      ab=lcaAll[k][a][b];  bx=lcaAll[k][b][x]; ax=lcaAll[k][a][x];
      if (!((ab==bx)&&(ax != ab))) return 0;
  }
  return 1;
}

char Ftriple(LABEL b, LABEL a, LABEL x)
/* check whether ax|b */ /* return 1 only if a, b, x are distinct */
{ int k; int ab, bx, ax;
  for (k=0; k<tree_num; k++)
  {
      ab=lcaAll[k][a][b];  bx=lcaAll[k][b][x]; ax=lcaAll[k][a][x];
      if (!((ab==bx)&&(ax == ab))) return 0;
  }
  return 1;
}

/* ================ Find MaxCweight (max clique) and next max clique =================== */
void printclique(char *msg);

void FindMaxC(LABEL a)
/* is called only when C(a, b) is ready and Csize > 0 */
{
    MaxCweight = 0;     /* size of max clique in G(a, b); */
    max_cliq_size = max_d - 2;
    cliq_id[1] = 0; cliqlen = 1; prefixweight =0;

  LABEL newid, newc; int i; char flag;

  while (1)
  {
          for (newid = cliq_id[cliqlen] + 1; newid <= Csize; newid++)
          {
             newc = C[newid];
             flag = 1;
             for (i=1; i < cliqlen; i++) if (Ftriple(a, cliq[i], newc) !=1) { flag = 0; break;}
              /* check Ftriple(a, x,y) to see whether (x, y) has an edge */
             if (flag) break;
          }
          if (newid <=Csize) /* a new clique found */
          {
              cliq_id[cliqlen] = newid; cliq[cliqlen] = newc; /* update newc at position cliqlen */

              if ((prefixweight + W[newid])> MaxCweight)
              {
                  MaxCweight = prefixweight + W[newid]; /* a new max clique find */
              }

              if ((cliqlen < max_cliq_size)&&(newid < Csize)) {prefixweight +=  W[newid]; cliq_id[++cliqlen] = newid;}
              /* grow the clique*/
              /* if cannot grow, next newid will be try*/
          }
          else /* no clique found */
          {
              if (cliqlen > 1) {prefixweight -=  W[cliq_id[--cliqlen]];} /* backtrack */
              else return; /* no more clique */
          }
  }
}

char next_maxclique(LABEL a) /* return 1 if exists */
{ LABEL newid, newc; int i; char flag;

  while (1)
  {
          for (newid = cliq_id[cliqlen] + 1; newid <= Csize; newid++)
          {
             newc = C[newid];
             flag = 1;
             for (i=1; i < cliqlen; i++) if (Ftriple(a, cliq[i], newc)!=1) { flag = 0; break;}
             if (flag) break;
          }
          if (newid <=Csize) /* a new clique found */
          {
              cliq_id[cliqlen] = newid; cliq[cliqlen] = newc; /* update newc at position cliqlen */

              if ((prefixweight + W[newid])== MaxCweight) return 1; /* a new max clique find */

              if ((cliqlen < max_cliq_size)&&(newid < Csize)) {prefixweight +=  W[newid]; cliq_id[++cliqlen] = newid;}
              /* grow the clique*/
              /* if cannot grow, next newid will be try*/
          }
          else /* no clique found */
          {
              if (cliqlen > 1) {prefixweight -=  W[cliq_id[--cliqlen]];} /* backtrack */
              else return 0; /* no more clique */
          }
  }
}

void init_maxclique1(LABEL a, LABEL b)
/* Csize, C[MAX_N], W[MAX_N] should be ready */
{
    MaxCweight = M[a][b]-M1[a][b]- M1[b][a]; /* size of max clique in G(a, b); */
    max_cliq_size = max_d - 2;
    cliq_id[1] = 0; cliqlen = 1; prefixweight =0;
}

/* =======for testing next max clique ==========*/
void printclique(char *msg)
{
       LABEL c;
       printf("%s (PreWeight = %d; total weight = %d; max weight = %d) ", msg, prefixweight, prefixweight + W[cliq_id[cliqlen]], MaxCweight);
       for (c=1; c<=cliqlen; c++) printf("[%d]", cliq[c]);
       printf("\n");
}

/* =============== MAST =================== */
void init_MAST() /* precompute the above */
{ int k, i, j;
    /* compute RTriple and FTriple */

    for(k = 0; k < tree_num; k++) /* for checking rooted/fan triples */
     if (k!=T0) {
       dfs_len = 0; makeLset(treestart[k], newroot[k]);
       //print_Lset(k);

       ComputeLCA_of_Tk(k);
     }

    dfs_len = 0; makeLset(treestart[T0], newroot[T0]); ComputeLCA_of_T0();

    // NewComputeRF();

    for (i=0; i<n; i++)
        for (j=0; j<n; j++) M[i][j] = M1[i][j] = 0; /*un-computed for avoiding recomputing */
}

void computeM1(LABEL a, LABEL b)
/* a <> b */
{   int subtree = childlist[node[lca[a][b]].first + LSubT[lca[a][b]-root0][a]]; /* the subtree containing a */
    int i, s; int max = 1; /* note that if X(a,b) is empty, set M1[a][b] = 1 for aa|b */
    LABEL c;

    if (subtree >=n)
    {
        s = subtree - treestart[T0];
        for (i=0; i<Lset[s].size; i++)
            if(Rtriple(b, a, c = Lset[s].memb[i]))
                if (M[a][c] > max) max = M[a][c];
    }
    M1[a][b] = max;
}

void MAST(LABEL a, LABEL b)
/* only for a <= b */
{
    LABEL c; int ca, cb;     int v, cc, i, child, lastplus1; TREENODE vnode;

    if (!M1[a][b]) computeM1(a, b); /* avoiding recomputing */
    if (!M1[b][a]) computeM1(b, a); /* avoiding recomputing */

    if ((max_d < 3)||(node[lca[a][b]].degree < 3)) Csize = 0;
    else {

            v=lca[a][b]; vnode = node[v]; ca=LSubT[v-root0][a]; cb=LSubT[v-root0][b];
            Csize = 0;
            for (cc = vnode.first, lastplus1 = vnode.first+vnode.degree; cc < lastplus1; cc++)
            {
                child = childlist[cc];
                if ((child!=ca)&&(child!=cb))
                {
                    if ((child = childlist[cc])<n)
                    { if (Ftriple(a, b, child)) {
                        if (!M1[child][a]) computeM1(child, a);
                        C[++Csize] = child; W[Csize] = M1[child][a];} continue; }

                    child -= treestart[T0];
                    for (i = 0; i < Lset[child].size; i++)
                    {
                        c = Lset[child].memb[i];
                        if (Ftriple(a, b, c)) { if (!M1[c][a]) computeM1(c, a); C[++Csize] = c; W[Csize] = M1[c][a]; }
                    }
                }
            }

/*        The following is slightly faster for binary trees
           Csize = 0;
           for(i = 0; i < n; i++)
              if(Ftriple(a, b, i))
              {
                if(!M1[i][a]) computeM1(i, a);
                C[++Csize] = i; W[Csize] = M1[i][a];
              }
*/
    }

    if (Csize==0) MaxCweight = 0; else FindMaxC(a);  /* Csize, C, and W are ready */

    M[a][b] = M[b][a]= M1[a][b] + M1[b][a] + (MC[a][b]=MC[b][a]= MaxCweight);
    /*avoid recomputing M(b, a) */

    if (M[a][b] > mast) mast = M[a][b];
}

SET L1, L2; /* used for Lset of a leaf in DFS */
void DFS_OnT()
/* s is internal */
{
   int i, j, x, y; SET L1, L2, *p1, *p2; int s, k;

   L1.size = L2.size = 1;
   for (k=0; k<dfs_len; k++)
   {
      s=dfs_order[k];
      for(i = 0; i < node[s].degree; i++)
        for(j = i+1; j < node[s].degree; j++){ /* pair of subtrees */
            LABEL child1 = childlist[node[s].first+i];
			if (child1<n) {L1.memb[0] = child1; p1=&L1;} else p1 = &Lset[child1-root0];
            LABEL child2 = childlist[node[s].first+j];
			if (child2<n) {L2.memb[0] = child2; p2=&L2;} else p2 = &Lset[child2-root0];
            for(x = 0;x < p1->size;x++)
                for(y = 0;y < p2->size;y++){ /* pair of nodes */
                    LABEL a = p1->memb[x];
                    LABEL b = p2->memb[y];
                    MAST(a, b);
                    //printf("call [%d][%d] done \n", a, b);
                 }
         }
   }
}

void MAST_MAIN()
/* compute max_d, T0, RTriple, FTriple, M, M1, mast */
{   LABEL a;
	init_MAST();
	for(a = 0;a < n;a++) M[a][a] = 1;
    mast = 0;
    DFS_OnT();
}

void UNROOT_MAST_MAIN()
/* compute max_d, T0, RTriple, FTriple, M, M1, mast */
{   LABEL f; int mm = -1;
    for (f=0; f < n; f++)
    {
      #if (DISPLAYROOT==1)
        printf("\nr=%d", f);
      #endif
      toRootedTrees(f);

	  MAST_MAIN();
	  if (mast > mm) mm = mast;
      toRootedTreesRecover();
      /*printf("root = %d; mast = %d \n", f, mast+1);*/
    }
    mast = mm + 1; /*including the leaf root */
}

/* =====================================KAST1===================================*/

void computeK1(LABEL a, LABEL b)
/* a <> b */
{   SET K1_ab; LABEL *Ma, mm = M1[a][b], x;
 //printf("***** Finding K1[%d][%d] \n", a, b);
    if (mm==1) {SET_KandK1_1(&K1[a][b], a); return;};

    Ma = M[a]; dmul_s_len = 0;
    for (x=0; x<a; x++)
        if (Rtriple(b, a, x) && (Ma[x]== mm)) dmul_s[dmul_s_len++] = &K[x][a];  /* x < a */
    for (x++; x<n; x++)                                             /* mm > 1, M(a, a) <> mm, skip */
        if (Rtriple(b, a, x) && (Ma[x]== mm)) dmul_s[dmul_s_len++] = &K[a][x];  /* a < x */
    DSET_mul_intersec(dmul_s_len, dmul_s, &K1_ab);
    SET_KandK1(&K1[a][b], &K1_ab);

#if (TESTING>=3)
       printf("===============K1[%d][%d] =", a, b); DSET_print("", &K1[a][b]);
#endif

}

void KAST1(LABEL a, LABEL b)
/* Note: called only when a <= b !!!!*/
{   SET K_ab, K1_ab, K1_ba, K_ca;
    SET s1, s2, s3, *result1, *result2, *temp, *cc;
    LABEL c, i;

    if (a==b) {SET_KandK1_1(&K[a][b], a); return;}; /* a = b may be moved outside this call */

    if (K1[a][b].size ==0) computeK1(a, b); /* Avoid recomputing K(a|b) */
    if (K1[b][a].size ==0) computeK1(b, a); /* Avoid recomputing */



    Csize = 0;   /* find C(a, b) and compute K(c|a); C[0] is unused */
    for (c =0; c < n; c++) if (Ftriple(a, b, c)&&(c!=leafroot))
    {
        if (K1[c][a].size ==0) computeK1(c, a); /* Avoid recomputing */
        C[++Csize] = c; W[Csize] = M1[c][a];
    }

    if (Csize == 0)
    {
        DSET_union(&K1[a][b], &K1[b][a], &K_ab);
        SET_KandK1(&K[a][b], &K_ab);


   #if (TESTING>=2)
        printf("*****K[%d][%d] =", a, b); DSET_print("", &K[a][b]);
   #endif


        return;
    } /* not assume disjoint */

    init_maxclique1(a, b);
    next_maxclique(a);

    result1 = &s1; result2 = &s2; temp = &s3;

    result1->size = 0;
    for (i=1; i<=cliqlen; i++)  /* 1st union */
         {
             DSET2SET(&K1[cliq[i]][a], &K_ca);
             SET_union(result1, &K_ca, temp); /* not assume disjoint */
             cc = temp; temp = result1; result1 = cc;
         }

    while (next_maxclique(a))  /* intersect with other unions */
    {
         result2->size = 0;
         for (i=1; i<=cliqlen; i++)
         {
             DSET2SET(&K1[cliq[i]][a], &K_ca);
             SET_union(result2, &K_ca, temp); /* not assume disjoint */
             cc = temp; temp = result2; result2 = cc;
          }

         SET_intersec(result1, result2, temp);
         cc = temp; temp = result1; result1 = cc;
      }

   #if (TESTING>=1)
        if (result1->size==0) {
                int i;
                printf("ERROR ! No maximum cliques of MaxCweight = %d are found for (a, b) = (%d, %d) !!!\n", MaxCweight, a, b);
                for (i=0; i< Csize; i++) printf("(C[%d], W[%d]) = (%d, %d)", i, i, C[i], W[i]);
                printf("\n");
        }
    #endif

    DSET2SET(&K1[a][b], &K1_ab); DSET2SET(&K1[b][a], &K1_ba);
    mul_ab[0] = &K1_ab; mul_ab[1] = &K1_ba; mul_ab[2] = result1;
    SET_mul_union(3, mul_ab, &K_ab); /* not assume disjoint */
    SET_KandK1(&K[a][b], &K_ab);


   #if (TESTING>=2)
        printf("*****K[%d][%d] =", a, b); DSET_print("", &K[a][b]);
   #endif

}

void FindKAST(SET *kast)
{ LABEL a, b;

  dmul_s_len = 0;
  for (a=0; a<n; a++)
    for (b=a+1; b<n; b++)
        if (M[a][b] == mast) dmul_s[dmul_s_len++] = &K[a][b];

  DSET_mul_intersec(dmul_s_len, dmul_s, kast);

}

void KAST1_DFS_OnT(int offset, int s)
/* s is internal */
{   int i, j, x, y, ss; SET L1, L2, *p1, *p2;

	L1.size = L2.size = 1; /* used for Lset of a leaf */

    for(i = 0; i < node[s].degree; i++)
        if ((ss=childlist[node[s].first+i])>= n) KAST1_DFS_OnT(offset, ss); /* DFS subtrees */
    for(i = 0; i < node[s].degree; i++)
        for(j = i+1; j < node[s].degree; j++){ /* pair of subtrees */
            LABEL child1 = childlist[node[s].first+i];
			if (child1<n) {L1.memb[0] = child1; p1=&L1;} else p1 = &Lset[child1-offset];
            LABEL child2 = childlist[node[s].first+j];
			if (child2<n) {L2.memb[0] = child2; p2=&L2;} else p2 = &Lset[child2-offset];
            for(x = 0;x < p1->size;x++)
                for(y = 0;y < p2->size;y++){ /* pair of nodes */
                    LABEL a = p1->memb[x];
                    LABEL b = p2->memb[y];
                    if (a<b) KAST1(a, b); else KAST1(b, a);
                }
        }
}

void KAST1_MAIN()
/* assume that MAST is done */
{   LABEL a, b;
    init_SETPOOL();
    for (a=0; a<n; a++)
        for (b=0; b<n; b++) K1[a][b].size = 0; /*indicating that this entry is uncomputed */
    for(a = 0;a < n;a++) SET_KandK1_1(&K[a][a], a);
    KAST1_DFS_OnT(treestart[T0], newroot[T0]);
    FindKAST(&kast);
    free_SETPOOL();
}

void UNROOT_KAST1_MAIN()
{ LABEL f; SET kast1; int mm = -1;
  for (f=0; f < n; f++)
  {
    #if (DISPLAYROOT==1)
      printf("\nr=%d", f);
    #endif

    toRootedTrees(f);
    MAST_MAIN();  /* note that mast not including the leaf root */

    if (mast < mm) {
            toRootedTreesRecover();
    /*        printf("Root = %d; mast = %d\n", f, mast+1); */
            continue;
    }

    KAST1_MAIN();
    kast.memb[kast.size] = f; kast.size++;  /* add the root f */

    if (mast > mm) {SET_copy(&kast1, &kast); mm = mast;} /* 1 st time */
    else {SET_intersec1(&kast1, &kast);}; /* mast = mm */

    /*printf("Root = %d; mast = %d; kast = %d\n", f, mast+1, kast1.size);*/
    toRootedTreesRecover();
  }

  SET_copy(&kast, &kast1);
  mast = mm + 1;  /* including the leaf root */
}

/* ===================================== KAST2 ===================================*/
void printC(LABEL a, LABEL b)
{  int i;
   printf("C set of (%d, %d) === ", a, b);
   for (i=1; i<=Csize; i++) printf("(%d, %d) ", C[i], W[i]);
   printf("\n");
}

void MakeCSimple(LABEL a, LABEL b)
{
    LABEL c;
    Csize = 0; /* find C(a, b) and compute K(c|a); C[0] is unused */
    for (c =0; c < n; c++)
        if (Ftriple(a, b, c)&&Relev[c][a]&&(c!=leafroot)) { C[++Csize] = c; W[Csize] = M1[c][a]; }
}

void MakeC(LABEL a, LABEL b)
{   LABEL c; int ca, cb;

    int v, cc, i, child, lastplus1; TREENODE vnode;
    v=lca[a][b]; vnode = node[v]; ca=LSubT[v-root0][a]; cb=LSubT[v-root0][b];

    Csize = 0;
    for (cc = vnode.first, lastplus1 = vnode.first+vnode.degree; cc < lastplus1; cc++)
    {
      child = childlist[cc];

      if ((child!=ca)&&(child!=cb))
      {
         if ((child = childlist[cc])<n)
         { if (Ftriple(a, b, child)&&Relev[child][a]) {C[++Csize] = child; W[Csize] = M1[child][a];} continue; }

         child -= treestart[T0];   /* relative id */
         for (i = 0; i < Lset[child].size; i++)
         {
            c = Lset[child].memb[i];
            if (Ftriple(a, b, c)&&Relev[c][a]) { C[++Csize] = c; W[Csize] = M1[c][a]; }
         }
      }
    }
}

LABEL *CH[MAX_D+1]; /* CH[i] pointers to the 1st C(a, b, H) in C(a, b), where CH[MAX_D] is a sentinel*/

void MakeCH(LABEL a, LABEL b) /* the following compute C(a, b) and C(a, b, H) */
/* is called only for relevant (a, b) and a!=b */
{   LABEL c;  int ca, cb;

    int v, cc, i, child, lastplus1; TREENODE vnode;
    v=lca[a][b]; vnode = node[v]; ca=LSubT[v-root0][a]; cb=LSubT[v-root0][b];

    Csize = 0;
    for (cc = vnode.first, lastplus1 = vnode.first+vnode.degree; cc < lastplus1; cc++)
    {
      CH[cc-vnode.first] = &C[Csize]+1;         /*pointer to 1st C(a, b, H) in C(a, b); */
      child = childlist[cc];
      if ((child!=ca)&&(child!=cb))
      {
          if ((child)<n)
           { if (Ftriple(a, b, child)) {C[++Csize] = child; W[Csize] = M1[child][a];} continue;}

          child -= treestart[T0];   /* relative id */

          for (i = 0; i < Lset[child].size; i++)
          {
            c = Lset[child].memb[i];
            if (Ftriple(a, b, c)) {
                    C[++Csize] = c; W[Csize] = M1[c][a];
            }
          }
      }
    }
    CH[vnode.degree] = &C[Csize]+1; /* sentinel */
}

char explored[MAX_N][MAX_N];

void AddRelevant(LABEL a, LABEL b);

void Explore(LABEL a, LABEL b)
/* explore pairs in X*(p|q) */
{  int subtree = childlist[node[lca[a][b]].first + LSubT[lca[a][b]-root0][a]]; /* the subtree containing a */
   int i, s; LABEL c;

   explored[a][b] = 1;

   if (subtree >=n)
   {
      s = subtree - treestart[T0];
      for (i=0; i<Lset[s].size; i++)
         if(Rtriple(b, a, c=Lset[s].memb[i]) && (M[a][c]==M1[a][b]) && (!Relev[a][c])) AddRelevant(a, c);
   }
}

void AddRelevant(LABEL a, LABEL b)
/* (a, b) is a new one a <> b  */
{
       LABEL c; char Cmax[MAX_N];

       Relev[a][b] = Relev[b][a] = 1;
       if (!explored[a][b]) Explore(a, b);
       if (!explored[b][a]) Explore(b, a);

       if (MC[a][b]==0) return;  /* Csize  = 0 */

       memset(Cmax, 0, n);
       MakeC(a, b);
       init_maxclique1(a, b);
       while (next_maxclique(a))
          for (c=1; c<=cliqlen; c++) Cmax[cliq[c]] = 1;
       for (c=0; c<n; c++) if ((Cmax[c]==1)&&(!explored[c][a])&&(c!=leafroot)) Explore(c, a);
}

void FindRelevant1();

void FindRelevant()
{
   LABEL a, b;

   for (a=0; a<n;a++) /*clear flag */
   {
       for (b=0;b<a;b++) Relev[a][b] = explored[a][b] = 0;
       Relev[a][b] = explored[a][b] = 1;  /* not explore (a, b) */
       for (b=a+1;b<n;b++) Relev[a][b] = explored[a][b] = 0;
   }

   for (a=0; a<n;a++)
       for (b=a+1;b<n;b++)
           if (M[a][b]==mast) AddRelevant(a, b);
}

/***********/
LABEL RQ[MAX_N*MAX_N][2]; int RQ_len;

void AddRelevant0(LABEL a, LABEL b)
{
  if (!Relev[a][b])
  {
      Relev[a][b] = Relev[b][a] = 1; RQ[RQ_len][0] = a; RQ[RQ_len++][1] = b; /* add (a, b) into RQ for exploring */
  }
}

void ExploreAB(LABEL a, LABEL b)
/* explore pairs in X*(p|q) */
{  int subtree = childlist[node[lca[a][b]].first + LSubT[lca[a][b]-root0][a]]; /* the subtree containing a */
   int i, s; LABEL c;
   explored[a][b] = 1;
   if (subtree >=n)
   {
      s = subtree - treestart[T0];
      for (i=0; i<Lset[s].size; i++)
         if(Rtriple(b, a, c=Lset[s].memb[i]) && (M[a][c]==M1[a][b])) AddRelevant0(a, c);
   }
}

void ExploreC(LABEL a, LABEL b)
/* a <> b  and Csize > 0 */
{      char Cmax[MAX_N];  /* move outside AddRelevant*/
       LABEL c;

       memset(Cmax, 0, n);
       MakeC(a, b);
       init_maxclique1(a, b);
       while (next_maxclique(a))
          for (c=1; c<=cliqlen; c++) Cmax[cliq[c]] = 1;
       for (c=0; c<n; c++) if (Cmax[c]&&(!explored[c][a])) ExploreAB(c, a);
}

void FindRelevant0()
{
   LABEL a, b;

   for (a=0; a<n;a++) /*clear flag */
   {
       for (b=0;b<a;b++) Relev[a][b] = explored[a][b] = 0;
       Relev[a][b] = explored[a][b] = 1;  /* not explore (a, b) */
       for (b=a+1;b<n;b++) Relev[a][b] = explored[a][b] = 0;
   }

   RQ_len = 0;
   for (a=0; a<n;a++)
       for (b=a+1;b<n;b++)
           if (M[a][b] == mast) AddRelevant0(a, b);

   while (RQ_len)
       {
           a = RQ[--RQ_len][0]; b = RQ[RQ_len][1];
           if (!explored[a][b]) ExploreAB(a, b);
           if (!explored[b][a]) ExploreAB(b, a);
           if (MC[a][b]) ExploreC(a, b); /* Csize  > 0 */
       }
}

/***********************/
void init_KAST2()
{   int a, t;

    /*MAST_MAIN(); move outside for handling unrooted trees*//* compute max_d, T0, RTriple, FTriple, M, M1, mast */

    for (a=0; a<n; a++)
        for (t=1; t<=n; t++) KK1[a][t].size = 0; /* KK1[a][t] needs 1st to initialize it */
                                                /* KK1[a][t] >=1, since at least containing a */
}

void UnionforKab(DSET *ab, DSET *ba, SET *c)
{   int i, newsize = 0;

    for (i = 0; i < ab->size; i++)
        if (SET_flag[ab->memb[i]] == 0)
        {
            K_ab.memb[newsize] = ab->memb[i];
            SET_flag[K_ab.memb[newsize++]] = 1;
        }
    for (i = 0; i < ba->size; i++)
        if (SET_flag[ba->memb[i]] == 0)
        {
            K_ab.memb[newsize] = ba->memb[i];
            SET_flag[K_ab.memb[newsize++]] = 1;
        }
    for (i = 0; i < c->size; i++)
        if (SET_flag[c->memb[i]] == 0)
        {
            K_ab.memb[newsize] = c->memb[i];
             SET_flag[K_ab.memb[newsize++]] = 1;
        }
      K_ab.size = newsize;
      for (i = 0; i < K_ab.size; i++) SET_flag[K_ab.memb[i]] = 0;

}

void KAST2(LABEL a, LABEL b)
/* Note: called only when a < b and (a, b) is relevant !!!!*/
{
    SET result1, result2;
    int i;

    if (MC[a][b]==0) { DSET_concate(&KK1[a][M1[a][b]], &KK1[b][M1[b][a]], &K_ab); return; }

    MakeC(a, b); /* Csize  >0 */

    init_maxclique1(a, b);
    /* initialize result1 */

    next_maxclique(a); dmul_s_len = 0;

    for (i=1; i<=cliqlen; i++) dmul_s[dmul_s_len++] = &KK1[cliq[i]][M1[cliq[i]][a]];
    DSET_mul_concate(dmul_s_len, dmul_s, &result1);

    while (next_maxclique(a))  /* intersect with other unions */
    {
         dmul_s_len = 0;
         for (i=1; i<=cliqlen; i++) dmul_s[dmul_s_len++] = &KK1[cliq[i]][M1[cliq[i]][a]];
         DSET_mul_concate(dmul_s_len, dmul_s, &result2);
         SET_intersec1(&result1, &result2);
    }

    #if (TESTING>=1)
        if (result1.size==0) {
                int i;
                printf("ERROR ! No maximum cliques of MaxCweight = %d are found for (a, b) = (%d, %d) !!!\n", MaxCweight, a, b);
                for (i=0; i< Csize; i++) printf("(C[%d], W[%d]) = (%d, %d)", i, i, C[i], W[i]);
                printf("\n");       }
    #endif

    UnionforKab(&KK1[a][M1[a][b]], &KK1[b][M1[b][a]], &result1);

#if (TESTING>=2)
    printf("*****K[%d][%d] =", a, b); DSET_print("", &K[a][b]);
#endif
}

void FindKAST2(SET *kast)
{ LABEL a, b;

  dmul_s_len = 0;
  for (a=0; a<n; a++)
    for (b=a+1; b<n; b++)
        if (M[a][b] == mast) {
            dmul_s[dmul_s_len++] = &KK1[a][mast];
            break;
            /* printf("(K[%d][%d] = ", a, b); SET_print("", &K[a][b]); */
        }

  DSET_mul_intersec(dmul_s_len, dmul_s, kast);
}

int my_comp_TRIPLE(const void *x, const void *y)
{
    return (((TRIPLE *)x) -> m - ((TRIPLE *)y) -> m);
}

void KAST2_MAIN()
/* call valid only if max_d > 2       */
{ LABEL a, b;
  int i;
  init_SETPOOL();
  init_KAST2(); FindRelevant0(); /*FindRelevant0();*/
  order_len = 0;
  /* compute K'(a, b) in order of m(a, b); only for a < b */
  for (a=0; a<n; a++)
    for (b=a+1; b<n; b++)
      if (Relev[a][b]) {order[order_len].a = a ; order[order_len].b = b ; order[order_len++].m = M[a][b];}
      /* else printf("(%d, %d) is not relevant\n", a, b);*/

  qsort(order, order_len, sizeof(TRIPLE), my_comp_TRIPLE);

  for(a = 0; a < n; a++) SET_KandK1_1(&KK1[a][1], a); /* KK1[a][1] = K[a][a] = "a" is done here */

  for (i=0; i<order_len; i++) {
        KAST2(a=order[i].a, b=order[i].b); /* a < b */

        if (!KK1[a][M[a][b]].size) SET_KandK1(&KK1[a][M[a][b]], &K_ab);
        else DSET_intersec1(&KK1[a][M[a][b]], &K_ab);  /* this may make some space of KK1[a][t] useless, but it is fine */
        if (!KK1[b][M[a][b]].size) SET_KandK1(&KK1[b][M[a][b]], &K_ab);
        else DSET_intersec1(&KK1[b][M[a][b]], &K_ab);
  }

  FindKAST2(&kast);
  free_SETPOOL();

}

void UNROOT_KAST2_MAIN()

{ LABEL f; SET kast1; int mm = -1;

  for (f=0; f < n; f++)
  {

    #if (DISPLAYROOT==1)
      printf("\nr=%d", f);
    #endif
    toRootedTrees(f);
    MAST_MAIN();  /* note that mast not including the leaf root */

    if (mast < mm) {
            toRootedTreesRecover();
            /*printf("Root = %d; mast = %d\n", f, mast+1);  */
            continue;
    }

    KAST2_MAIN();
    kast.memb[kast.size] = f; kast.size++;  /* add the root f */

    if (mast > mm) {SET_copy(&kast1, &kast); mm = mast;} /* 1 st time */
    else {SET_intersec1(&kast1, &kast);}; /* mast = mm */

    /*printf("Root = %d; mast = %d; kast = %d\n", f, mast+1, kast1.size);*/

    toRootedTreesRecover();
  }

  SET_copy(&kast, &kast1);
  mast = mm + 1;  /* including the leaf root */
}

/* ===================================================== */
int main(int argc, char* argv[])
{ clock_t tstart, tp, ti, to; double time_i, time_p, time_o;
  int test_rounds = default_test_rounds, iter, i;
  char *inputfile, tree_type, function_type;

    if (argc==1) {
            inputfile = Default_InputFileName;
            tree_type = Default_tree_type;
            function_type = Default_Function2Run;
    }
    else if (argc == 4) { inputfile = argv[1]; tree_type = atoi(argv[2]); function_type =  atoi(argv[3]); }
    else { printf("\nIncorrect Usage !!!\n");
           printf("Three arguments (filename tree_type function2run) required.\n");
           printf("tree_type: 0/1 for unrooted/rooted trees\n");
           printf("function2run: 0/1/2 for MAST/KAST1/KAST2\n");
           return 0;
         }

    tstart = clock();

    if (get_input(inputfile)) return 0;

	printf("\nFile: %s, (tree_num, n, d) = (%d, %d, %d)\n", inputfile, tree_num, n, max_d);

    if (tree_type == 0)
    {
        init_unrooted_trees();
        if (function_type ==  0) printf("Processing (UNROOTED MAST: %d rounds) ....", default_test_rounds);
        if (function_type ==  1) printf("Processing (UNROOTED KAST1: %d rounds) ....", default_test_rounds);
        if (function_type ==  2) printf("Processing (UNROOTED KAST2: %d rounds) ....", default_test_rounds);
    } else {
        init_rooted_trees();
        if (function_type ==  0) printf("Processing (ROOTED MAST: %d rounds) ....", default_test_rounds);
        if (function_type ==  1) printf("Processing (ROOTED KAST1: %d rounds) ....", default_test_rounds);
        if (function_type ==  2) printf("Processing (ROOTED KAST2: %d rounds) ....", default_test_rounds);
    }

    for (i=0; i < n; i++) {
            K[i] = malloc(n*sizeof(DSET));
            K1[i] =malloc((n+1)*sizeof(DSET));  /* KK1[a][t] needs t=0~n */
            if ((K[i]==NULL)||(K1[i]==NULL))
            {
                printf("\nMemory allocation fails!!!...........\n"); return 0;
            }
    }

   	ti = clock();
    kast.size=0;
    for (iter=0, ti = clock(); iter < test_rounds; iter++)
    {
        #if (TESTING>=1)
            set_op_cnt = 0; set_op_total_size = 0;
        #endif

        if (tree_type == 0)
        {
            if (function_type ==  0) UNROOT_MAST_MAIN();
            if (function_type ==  1) UNROOT_KAST1_MAIN();
            if (function_type ==  2) UNROOT_KAST2_MAIN();
        } else {
            if (function_type ==  0) MAST_MAIN();
            if (function_type ==  1) {MAST_MAIN(); KAST1_MAIN();}
            if (function_type ==  2) {MAST_MAIN(); KAST2_MAIN();}
        }
	}
    printf("\ndone.\n");
    if (function_type>=0) printf("mast = %d\n", mast);
    if (function_type>=1) SET_print("kast = ", &kast);

	tp=clock();

    output();

	to = clock();
	printf("\n");
#if (TESTING>=1)
    printf("Total Set Operations: %lu; Total Size: %lu\n", set_op_cnt, set_op_total_size);
#endif
    time_p = (double)(tp - ti)/CLOCKS_PER_SEC/test_rounds;
	time_i = (double)(ti - tstart) / CLOCKS_PER_SEC;
	time_o = (double)(to - tp) / CLOCKS_PER_SEC;
    printf("Time: (Exe (%d rounds), In/Out, Total) = (%.5lfs, %.5lfs/%.5lfs, %.5lfs)\n",
        test_rounds, time_p, time_i, time_o, time_i+time_p+time_o);

    return 0;
}

