__Compiling__

Compile kast.c with these flags (or use the Makefile):

`gcc t-mcmodel=large -m64`



------------------------------------------------------


__Running RAxML__:

_Rapid Bootstrap with ML inference_

`raxmlHPC -T NUM_THREADS -f a -m GTRGAMMA -p 12345 -x 12345 -# 100 -s IN_ALIGNMENT -n OUTPUT_NAME`

_Rapid Bootstrap only_

`raxmlHPC -T NUM_THREADS -m GTRGAMMA -p 12345 -x 12345 -# 100 -s IN_ALIGNMENT -n OUTPUT_NAME`

_Slow ML inference_

`raxmlHPC -T NUM_THREADS -m GTRGAMMA -p 12345 -# NUM_SEARCHES -s IN_ALIGNMENT -n OUTPUT_NAME`

_Slow bootstrap_

`raxmlHPC -T NUM_THREADS -m GTRGAMMA -p 12345 -b 12345 -# 100 -s IN_ALIGNMENT -n OUTPUT_NAME`


(https://sco.h-its.org/exelixis/web/software/raxml/hands_on.html)


------------------------------------------------------

__Test ML__:

KH method
 - test if two likelihoods are significantly different
