# coding: utf-8

import sys
import os
from collections import defaultdict
import re
import glob

from ast import literal_eval as make_tuple
from itertools import combinations, chain

configfile: "snakeconfig/test.yaml"


#### SWITCHES ####

DO_NNI = config['DO_NNI']
DO_WANDER = config['DO_WANDER']
DO_RANDOMUNI = config['DO_RANDOMUNI']

USE_PERCENTAGE = config['USE_PERCENTAGE']

SKIP_OLD = config['SKIP_OLD']
SKIP_NEW = config['SKIP_NEW']

USE_ROOT = config['USE_ROOT']

PARALLEL = config['PARALLEL']

#### BASIC SETTINGS ####

NUM_TREES = config['NUM_TREES']

NUM_LEAF_RANGE = config['NUM_LEAF_RANGE']
NUM_LEAF_STEP = config['NUM_LEAF_STEP']
NUM_REPS = config['NUM_REPS']

NUM_MUTSTEPS = config['NUM_MUTSTEPS']
MUTATION_STEP = config['MUTATION_STEP']

TIME_PREF = config['TIME_PREF']
TIMINGS_DIR = config['TIMINGS_DIR']

PLOT_PREF = config['PLOT_PREF']
PLOTS_DIR = config['PLOTS_DIR']
 

#### NOT IMPLEMENTED:
if 'NUM_TREE_STEP' in config:
  NUM_TREE_STEP = config['NUM_TREE_STEP']
else:
  NUM_TREE_STEP = 0
if 'NUM_TREE_MAX' in config:
  NUM_TREE_MAX = config['NUM_TREE_MAX']
####################


#### CONSTANTS ####

SKIPOLD_STR = 'skipold'
SKIPNEW_STR = 'skipnew'
SKIPBOTH_STR = 'skipboth'

# Gather KAST executables:

KAST_DIR = 'runningtime'



################################################################################

def gatherKastExecs():
  """
  Make a dictionary that organizes kast executables by number of leaves and then
  number of trees.
  """
  leavesTOtreesTOkast = defaultdict(dict)
  kre = re.compile(r'kast_(\d+)_(\d+)')
  for kexec in glob.glob(f'{KAST_DIR}/kast_*_*'):
    m = kre.search(kexec)
    if m:
      numleaves = int(m.group(1))
      numtrees = int(m.group(2))
      leavesTOtreesTOkast[numleaves][numtrees] = f'kast_{numleaves}_{numtrees}'

  if not len(leavesTOtreesTOkast):
    raise(Exception(f'No kast executable found at "{KAST_DIR}/"!\n'+
                    '\texpecting executable of the form "kast_3000_10" where '+
                    '3000 is the number of leaves and 10 the number of trees.'))

  return leavesTOtreesTOkast
leavesTOtreesTOkast = gatherKastExecs()

def chooseKASTexec(numleaves, numtrees):
  """
  Choose the smallest KAST executable that can handle the given number of
  leaves and trees.

  @return: The string of the kast executable or and empty string.
  """
  for leaves in sorted(leavesTOtreesTOkast):
    if numleaves <= leaves:
      for trees in sorted(leavesTOtreesTOkast[leaves]):
        if numtrees <= trees:
          return leavesTOtreesTOkast[leaves][trees]

  return ''


def getDir(basedir, mutationstr):
  """
  Return the directory name for this run.
  """
  skipstr = ''
  if SKIP_OLD:
    if SKIP_NEW:
      skipstr = SKIPBOTH_STR
    else:
      skipstr = SKIPOLD_STR
  elif SKIP_NEW:
    skipstr = SKIPNEW_STR
  else:
    skipstr = 'withold'

  rootstr = 'NOROOT'
  if USE_ROOT:
    rootstr = 'ROOT'

  #hostname = socket.gethostname()

  if NUM_LEAF_RANGE[0] > NUM_LEAF_RANGE[1]:
    raise Exception(f'Invalid value NUM_LEAF_RANGE = {NUM_LEAF_RANGE}')
  
  return f'{basedir}/{skipstr}/{mutationstr}_{NUM_TREES}_'+\
         f'{NUM_LEAF_RANGE[0]}-{NUM_LEAF_RANGE[1]}_'+\
         f'{NUM_MUTSTEPS}_{MUTATION_STEP}_{NUM_REPS}_{rootstr}/'


def buildTargetList(wildcards):
  """
  Build the list of ultimate targets based on the current set of constants.
  """
  filelist = []
  if DO_NNI:
    filelist += getFilesFor('NNI')
    filelist += getPlotFilesFor('NNI')
    filelist.append(f'{getDir(TIMINGS_DIR, "NNI")}logfile.txt')

  if DO_WANDER:
    filelist += getFilesFor('WANDER')
    filelist += getPlotFilesFor('WANDER')
    filelist.append(f'{getDir(TIMINGS_DIR, "WANDER")}logfile.txt')

  if DO_RANDOMUNI:
    filelist += getFilesFor('RANDOMUNI')
    filelist += getPlotFilesFor('RANDOMUNI')
    filelist.append(f'{getDir(TIMINGS_DIR, "RANDOMUNI")}logfile.txt')

  return filelist


def getFilesFor(mutationstr):
  skipstr = ''
  if SKIP_OLD:
    if SKIP_NEW:
      skipstr = SKIPBOTH_STR
    else:
      skipstr = SKIPOLD_STR
  elif SKIP_NEW:
    skipstr = SKIPNEW_STR
  else:
    skipstr = 'withold'

  rootstr = 'NOROOT'
  if USE_ROOT:
    rootstr = 'ROOT'

  filenamespec = getDir(TIMINGS_DIR, mutationstr)+\
                 f'{TIME_PREF}_{mutationstr}_{NUM_TREES}_'+\
                 f'{{nleaves}}-{NUM_MUTSTEPS}'+\
                 f'-{MUTATION_STEP}_{rootstr}_{{rep}}.txt'

  filelist = []
  for numleaves in range(*NUM_LEAF_RANGE, NUM_LEAF_STEP):
    filelist += expand(filenamespec, nleaves=numleaves, rep=range(NUM_REPS))

  return filelist


def getPlotFilesFor(mutationstr):
  skipstr = ''
  if SKIP_OLD:
    if SKIP_NEW:
      skipstr = SKIPBOTH_STR
    else:
      skipstr = SKIPOLD_STR
  elif SKIP_NEW:
    skipstr = SKIPNEW_STR
  else:
    skipstr = 'withold'
  
  rootstr = 'NOROOT'
  if USE_ROOT:
    rootstr = 'ROOT'

  timeplotspec = getDir(PLOTS_DIR, mutationstr)+\
                 f'{PLOT_PREF}_time_{mutationstr}_{NUM_TREES}_{rootstr}_{{iteration}}.pdf'
  sizeplotspec = getDir(PLOTS_DIR, mutationstr)+\
                 f'{PLOT_PREF}_size_{mutationstr}_{NUM_TREES}_{rootstr}_{{iteration}}.pdf'

  filelist = []
  for i in range(0, NUM_MUTSTEPS):
    filelist += expand(timeplotspec, iteration=i)
    filelist += expand(sizeplotspec, iteration=i)

  return filelist



rule all:
  input:
    buildTargetList,


rule make_logfile:
  output: '{dir}/logfile.txt'
    
  shell:
    'date >> {output}; echo $HOSTNAME >> {output}'



rule do_timing:
  """
  Run "timekast -n" on a fixed set of parameters.
  """
  input:
  output:
    TIMINGS_DIR+'/{skipstr}/{dir}/'+TIME_PREF+'_{mutmethod}_{ntrees}_{nleaves}-{numsteps}-{step}_{rootstr}_{rep}.txt',

  run:
    switches = ''
    if wildcards['skipstr'] == SKIPOLD_STR:
      switches += '--skip-old '
    elif wildcards['skipstr'] == SKIPNEW_STR:
      switches += '--skip-new '
    elif wildcards['skipstr'] == SKIPBOTH_STR:
      switches += '--skip-new --skip-old '

    if wildcards['rootstr'] == 'NOROOT':
      switches += '--unrooted '

    if PARALLEL:
      switches += '--parallel '

    step = wildcards.step
    if USE_PERCENTAGE:
      step = int(float(step)*float(wildcards.nleaves))

    if wildcards['mutmethod'] == 'NNI':
      methodswitch = f'-n {wildcards.numsteps} {step}'
    elif wildcards['mutmethod'] == 'WANDER':
      methodswitch = f'-w {wildcards.numsteps} {step}'
    elif wildcards['mutmethod'] == 'RANDOMUNI':
      methodswitch = '-r'

    numleaves = int(wildcards.nleaves)
    if USE_PERCENTAGE:
      numleaves = int(wildcards.nleaves) + step

    kast_exec = chooseKASTexec(numleaves, int(wildcards.ntrees))
    shell('./timekast {switches} -e {kast_exec} -k {wildcards.ntrees} {wildcards.nleaves} {methodswitch} > {output}')



def buildPlotInputFilesList(wildcards):
  return getFilesFor(wildcards.mutmethod)

rule make_time_plot:
  """
  Make the timing plots for a given dataset.
  """
  input:
    buildPlotInputFilesList,
  output:
    PLOTS_DIR+'/{skipstr}/{dir}/'+PLOT_PREF+'_time_{mutmethod}_{numtrees}_{root}_{iteration}.pdf',

  shell:
    './plottimes -l {wildcards.iteration} {output} {TIMINGS_DIR}/{wildcards.skipstr}/{wildcards.dir}/timing_*.txt'

rule make_size_plot:
  """
  Make the AST size plots for a given dataset.
  """
  input:
    buildPlotInputFilesList,
  output:
    PLOTS_DIR+'/{skipstr}/{dir}/'+PLOT_PREF+'_size_{mutmethod}_{numtrees}_{root}_{iteration}.pdf',

  run:
    plot_kast = ''
    if not (SKIP_NEW and SKIP_OLD):
      plot_kast = '-k'

    shell('./plottimes {plot_kast} -m -i --dev -l {wildcards.iteration} {output} {TIMINGS_DIR}/{wildcards.skipstr}/{wildcards.dir}/timing_*.txt')
