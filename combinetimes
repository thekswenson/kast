#!/usr/bin/python3
"""
Take timings done from different runs and combine them.
"""
import sys
import os
import re
import glob
import argparse
from ast import literal_eval




#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|  FUNCTIONS  |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def getInfoFromFilename(infile):
  """
  Extract information from the timing filename.

  @return: (mutmethod, leaves, numsteps, step) where mutmethod is the mutation
           method for the tree (e.g. "NNI" or "WANDER"), leaves is the number of
           leaves, numsteps is the number of times step mutations have been
           applied to the trees.
  """
  m = re.match(r'.*timing_(\w+)_\d+_(\d+)-(\d+)-(\d+\.\d+|\d+)_\w+_\d+.txt', infile)
  if not m:
    form = '\texpected: timing_NNI_#trees_#leaves-#steps-step_ROOT_#rep.txt'
    raise Exception(f'Unexpected filename format for file {infile}:\n{form}')

  if '.' in m.group(4):
    step = float(m.group(4))
  else:
    step = int(m.group(4))

  return m.group(1), int(m.group(2)), int(m.group(3)), step



#______________________________________________________________________________#
#|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#
#|:=--=:|     Main    |:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|:=--=:|#


def main():
  desc = 'Combine timing from different runs of timekast.'
  parser = argparse.ArgumentParser(description=desc)

  parser.add_argument('MAST_TIMES',
                      help="the directory with the MAST timings.")
  parser.add_argument('OLD_KAST_TIMES',
                      help="the directory with the new KAST timings.")
  parser.add_argument('NEW_KAST_TIMES',
                      help="the directory with the new KAST timings.")
  parser.add_argument('NEW_DIR',
                      help="the directory to create.")

  args = parser.parse_args()

  mast_dir = args.MAST_TIMES
  oldk_dir = args.OLD_KAST_TIMES
  newk_dir = args.NEW_KAST_TIMES
  new_dir = args.NEW_DIR

  if os.path.exists(new_dir):
    sys.exit(f'Given dir "{new_dir}" already exists.')

  os.mkdir(new_dir)

#MAIN:    __    __    __    __    __    __    __    __    __    __    __    __
#__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \__/  \_

  for path in glob.glob(f'{mast_dir}/timing_*'):
    filename = os.path.basename(path)
    with open(path) as f:
      mastt, _, _ = literal_eval(f'[{f.readline().rstrip()}]')
    with open(f'{oldk_dir}/{filename}') as f:
      _, kast1t, _ = literal_eval(f'[{f.readline().rstrip()}]')
    with open(f'{newk_dir}/{filename}') as f:
      _, _, kast2t = literal_eval(f'[{f.readline().rstrip()}]')

    with open(f'{new_dir}/{filename}', 'w') as f:
      f.write(f'{mastt}, {kast1t}, {kast2t}')


if __name__ == "__main__":
  main()
